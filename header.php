<!DOCTYPE html>
<html lang="hu">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo wp_title( '|', true, 'right' ); ?></title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php wp_head(); global $post; ?>
  </head>
  <body <?php body_class(); ?>>
	
	<header>
		<nav class="navbar">
			<div class="container">
				<div class="col-sm-2">
					<div class="row">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="/wp-content/uploads/2017/01/wiki_logo3-300x201.png" class="site_logo" ></a>
					</div>
				</div>
				
				<div class="col-sm-5 search_field">
					<input type="text" id="search_inp" class="form-control search_inp" placeholder="Keresés" />
                    <button class="btn btn-default pull-right" href="#" role="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>									
				</div>
				
				<div class="col-sm-1"></div>
				
				<div class="col-sm-4 well well-sm search_parameters">
					<div class="col-sm-6 col">
						<ul>
							<li>
								<input type="checkbox" value="post_cim" id="post_cim" />
								<label for="post_cim">Post cím</label>									
							</li>
							<li>
								<input type="checkbox" value="keresoszavak" id="keresoszavak" />
								<label for="keresoszavak">Keresőszavak</label>									
							</li>
							<li>
								<input type="checkbox" value="tartalom" id="tartalom" />
								<label for="tartalom">Tartalom</label>									
							</li>							
						</ul>
					</div>				
					<div class="col-sm-6 col">
						<ul>
							<li>
								<input type="checkbox" value="prog_kapcs" id="prog_kapcs" />
								<label for="prog_kapcs">Programozással kapcs.</label>									
							</li>
							<li>
								<input type="checkbox" value="cgi_content" id="cgi_content" />
								<label for="cgi_content">3D-vel kapcsolatos tartalm.</label>									
							</li>
						</ul>
					</div>			
				</div>
			</div>				
		</nav>
	</header>		
	<div class="clearfix"></div>	
		
	<div id="content_wrapper" class="container">
		<div class="row row-eq-height">