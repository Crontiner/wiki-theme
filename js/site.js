jQuery(function($) {


	function get_current_page_id() {
		var bodyClasses = $('body').attr('class');
		bodyClasses = bodyClasses.split(' ');

		var post_id = 0;
		$( bodyClasses ).each(function(key, class_item){
			if ( class_item.indexOf("postid-") ) {
				class_item = class_item.replace(/[^0-9]/g, '');
			} else if ( class_item.indexOf("page-id-") ) {
				class_item = class_item.replace(/[^0-9]/g, '');
			}

			if ( parseInt(class_item) > 0 ) {
				post_id = class_item;
			}
		});

		return parseInt(post_id);
	}


	$('header nav input.search_inp').click(function () {

	});


	$('header nav input.search_inp').keyup(function(event) {
	  if (event.which == 13) {
	      kereso();
	  }
	});

	$('header .search_field button').click(function(){
		kereso();
	});

	$('div.search_parameters input').on('ifChanged', function(event){
		$('section#search_results').attr('data-search-condition', "");
	});

	function kereso() {
		var $search_inp = $('header nav input.search_inp');

		if ( $search_inp.val() != "" ) {
			if ( $('section#search_results').attr('data-search-condition') != $search_inp.val() ) {
				var hash = Math.floor((Math.random() * 99999) + 1);

				/* keresési feltételek lekérése */
				var post_cim = $('div.search_parameters #post_cim').prop('checked');
				var keresoszavak = $('div.search_parameters #keresoszavak').prop('checked');
				var tartalom = $('div.search_parameters #tartalom').prop('checked');
				var prog_kapcs = $('div.search_parameters #prog_kapcs').prop('checked');
				var cgi_content = $('div.search_parameters #cgi_content').prop('checked');

				var searchTermsObj = { 	post_cim : post_cim,
										keresoszavak : keresoszavak,
										tartalom : tartalom,
										prog_kapcs : prog_kapcs,
										cgi_content : cgi_content,
									};

				if ( $('div#search_results_spinner').length < 1 ) {
					$( '<div id="search_results_spinner" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' ).insertBefore( $( "section#search_results" ) );
				}

				jQuery.post(
					wiki.ajaxurl,
					{
						search_value : $search_inp.val(),
						search_terms : searchTermsObj,
						hash : hash,
						action : 'search_results'
					},
					function(data) {
						if (data.hash == hash) {

							$('section#search_results').html( data.html ).attr('data-search-condition', $search_inp.val());
							$('div.page-content-section').fadeOut();
							$('div#sidebar_wrapper .search-results-widget').remove();

							/* találattok kategóriáinak sorrendezése nézettség alapján */

							// nézettség összeadása

							$('section#search_results .panel').each(function(){

								var panelViews = 0;
								$(this).find('ul li').each(function(){
									var $this = $(this);

									panelViews = panelViews + parseInt($this.find('span.views').html());
								});

								$(this).attr('data-views', panelViews);
							});

							// sorbarendezés nézettség szerint

							var nezettseg_szerint_rendez = [];
							var $search_results = $('section#search_results .panel');

							$search_results.each(function() {
								var $this = $(this);
								var nezettseg = parseInt($this.attr('data-views'));
								var term_id = parseInt($this.attr('data-term-id'));
								var $li = $this.parent().html();
								var valueToPush = new Array();

								valueToPush[0] = term_id;
								valueToPush[1] = nezettseg;

								nezettseg_szerint_rendez.push(valueToPush);
							});

							nezettseg_szerint_rendez.sort(function (a, b) {
							  var column = 1;

							  if (a[column] > b[column]) {
							    return 1;
							  }
							  if (a[column] < b[column]) {
							    return -1;
							  }

							  return 0;
							});

							nezettseg_szerint_rendez = nezettseg_szerint_rendez.reverse();

							var nezettseg_sc_ul_result = '';
							for (i = 0; i < nezettseg_szerint_rendez.length; i++) {
							    nezettseg_sc_ul_result += $search_results.closest('section').find('.panel[data-term-id="'+nezettseg_szerint_rendez[i][0]+'"]').get(0).outerHTML;
							}
							$search_results.closest('section').html(nezettseg_sc_ul_result).fadeIn();

							$('div#search_results_spinner').remove();

							/* colorize the highlight headings */

							if ( data.colored_search_terms != "" ) {
								$( '<div class="hidden">'+ data.colored_search_terms +'</div>' ).appendTo( 'section#search_results' );
								var $colored_search_terms = $('ul#colored_search_terms');

								$('section#search_results .panel b.highlight').each(function() {
									var $this = $(this);
									var lowerString = $this.html().toLowerCase();
									var color = "";

									//$this.attr('data-slug', $this.html().toLowerCase());

									$colored_search_terms.find('li').each(function() {
										var $li = $(this);

										if ( $li.attr('data-search-term') == lowerString ) {
											color = $li.attr('data-color');
										}
									});

									$this.css('color', '#'+ color);
								});
							}

							/* widget összerakása */

							if ( $('section#search_results .panel').length > 1 ) {

								if ( $('div#sidebar_wrapper .search-results-widget').length ) {  }
								else {
									$('div#sidebar_wrapper').prepend('<div class="widget search-results-widget"><h3 class="wg-title"><span>Találatok</span></h3><ul></ul><div class="ossz"></div></div>');
								}

								$('div#sidebar_wrapper .search-results-widget ul').html('');

								$('section#search_results .panel .panel-heading .panel-title').each(function() {
									var $this = $(this);
									var x = Math.floor((Math.random() * 99999) + 1);

									$this.attr('id', 'id-'+ x);
									$('div#sidebar_wrapper .search-results-widget ul').append( '<li><a href="#id-'+ x +'">'+ $(this).html() +'<span>'+ $this.closest('.panel').find('ul.list-group li').length +'</span></a></li>' );
								});

								$('div#sidebar_wrapper .search-results-widget .ossz').html('Összes találat: <b>'+ $('section#search_results .panel .panel-body ul.list-group li.list-group-item').length +'</b>' );
							}

						}
					},'json'
				);

			}
		} else {
			$('div.page-content-section').show();
			$('section#search_results').hide();
			$('div#sidebar_wrapper .search-results-widget').remove();
			$('div#search_results_spinner').remove();
		}
	}


	/*
	$(document).ready(function() {
	  $('body.single pre').each(function(i, block) {
	    hljs.highlightBlock(block);
	  });
	});
	*/

	if($("input[type='checkbox'], input[type='radio']").length){
		$("input[type='checkbox'], input[type='radio']").iCheck({
			checkboxClass: 'icheckbox_minimal-aero',
			radioClass: 'iradio_minimal-aero',
			increaseArea: '20%'
		});

		$('input#post_cim').iCheck('check');
		$('input#keresoszavak').iCheck('check');
		$('input#prog_kapcs').iCheck('check');
	}


	$(window).load(function(){
		$('body.home header input#search_inp').focus();
	});

	setTimeout(function() {
		$('.wp-embedded-content').remove();
	}, 500);

	$('div.pagination_content nav.pagination ul li span.current').parent().addClass('active');


	/* Nézettség hozzáadása */
	var hash = Math.floor((Math.random() * 99999) + 1);
	jQuery.post(
			wiki.ajaxurl,
			{
				post_id : get_current_page_id(),
				hash : hash,
				action : 'bejegyzes_nezettseg_frissites'
			},
			function(data) {
					if (data.ready == 'true') {
						console.dir(data.ready);
					}
			},'json'
	);


});
