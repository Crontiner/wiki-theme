<?php get_header(); ?>

	<div id="page_content" class="col-sm-9">

		<section id="search_results">
			<!-- Search results here... -->
		</section>

		<div class="page-content-section">
			<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					$postID = get_the_ID();
	
					$has_thumbnail_class = '';
					$header_thumbnail = '';
					if ( has_post_thumbnail() ) {
						$has_thumbnail_class = 'has_thumbnail';
	
						$array = wp_get_attachment_image_src( get_post_thumbnail_id( $postID ), 'large' );
						$header_thumbnail = 'background: url('.$array[0].');';
					}
					?>
					<article class="<?php echo $has_thumbnail_class; ?>">
						<div class="header <?php echo $has_thumbnail_class; ?>" style="<?php echo $header_thumbnail; ?>">
							<h2 class="title"><a href="<?php echo get_the_permalink( $postID ); ?>"><?php echo get_the_title(); ?></a></h2>
						</div>						
						<div class="desc"><?php echo get_the_excerpt_by_id($postID, true); ?></div>
					</article>				
					<?php
				} // end while
			} // end if
			?>
		</div>	
			
		<?php echo '<div class="pagination_content">' . wp_pagenavi(array('echo' => false)) . '</div>'; ?>	
	</div>

	<?php get_template_part('template-parts/sidebar'); ?>

<?php get_footer(); ?>