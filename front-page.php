<?php get_header(); ?>

	<?php 
	$main_terms_array = get_option( 'frontpage_post_list_order' );
	$main_terms_array = $main_terms_array['result'];
	?>

	<div id="page_content" class="col-sm-9">
	
		<div id="all_category_list">	
			
			<section id="search_results">
				<!-- Search results here... -->
			</section>			
	
		<?php 
			foreach ($main_terms_array as $key1 => $terms) {
				
				?><div class="row page-content-section"><?php
		
					foreach ($terms as $key2 => $term) {
						?>
						<div class="col-sm-4">
							<div class="panel panel-default">
							  <span></span><span></span><span></span>
							  <div class="panel-heading">
							  	<h3><a href="<?php echo $term['term_url']; ?>"><?php echo $term['term_name']; ?></a></h3>
							  </div>


								<?php 
								$child_terms = get_terms( 	'category', array(
														    'orderby' => 'count',
														    'order'	=> 'DESC',
														    'hide_empty' => TRUE,
														    'parent' => $term['term_id']
														) );
								$child_terms = array_slice($child_terms, 0, 10);
								$terms_list = "";

								if ( count($child_terms) >= 5 ) {
									$child_terms = array_chunk($child_terms, 2);

									foreach ($child_terms as $key1 => $c_terms) {
										$terms_list .= '<div class="col-xs-6"><ul>';
											$term_list = "";
											foreach ($c_terms as $key2 => $c_term) {
												$term_list .= '<li><a href="'.get_term_link( $c_term ).'">'. $c_term->name .'</a></li>';
											}
											$terms_list .= $term_list;
										$terms_list .= '</ul></div>';
									}

								} else if ( count($child_terms) > 0 ) {
										
									$terms_list = "";
									foreach ($child_terms as $key => $c_term) {
										$term_list .= '<li><a href="'.get_term_link( $c_term ).'">'. $c_term->name .'</a></li>';
									}
									$terms_list .= '<div class="col-xs-12"><ul>'. $term_list .'</ul></div>';
								}
								?>
									
								<?php if ( !empty($terms_list) ) { ?>									
								<div class="panel-body terms_list">						
									<?php echo $terms_list; ?>							
								</div>
								<?php } ?>
								
								<!-- -->
								
								<?php
								
								$popular_posts = new WP_Query(array( 'post_type' => 'post', 
																	'fields' => 'ids', 
																	'posts_per_page' => 5,
																	'meta_key'   => 'megnezve',
																	'orderby'    => 'meta_value_num',
																	'order'      => 'DESC',																		
																	'tax_query' => array(
																			array(
																				'taxonomy' => 'category',
																				'field'    => 'term_id',
																				'terms'    => array( $term['term_id'] ),
																			),
																		),						
								));
								$popular_posts_array = $popular_posts->posts;									
								?>				
				
								<!-- List group -->
								<ul class="list-group posts">
									<?php
									foreach ($popular_posts_array as $key => $post_id) {
										echo '<li class="list-group-item"><a target="_blank" href="'. get_permalink( $post_id ) .'">'. get_the_title( $post_id ) .'</a></li>';
									}
									?>
								</ul>
								
							</div>
						</div>
						<?php
					}
							
				?></div><?php
			}
		?>		
		
		</div><!-- /#all_category_list -->
			
	</div>

	<?php get_template_part('template-parts/sidebar'); ?>
	
<?php get_footer(); ?>