<?php

/*******************************/
/*      GLOBAL VARIABLES       */
/*******************************/

define( 'TEMPLATE_DIRECTORY_URI', get_template_directory_uri() );
define( 'TEMPLATE_DIRECTORY', get_template_directory() );
define( 'STYLESHEET_DIRECTORY', get_bloginfo('stylesheet_directory') );

define( 'BLENDER_TERM_ID', 1329 );
define( 'FRONT_PAGE_ID', 4166 );

/**/

/**************************/
/*    SETTINGS FOR WP     */
/**************************/

add_theme_support( 'post-thumbnails' );

/**/

/************************/
/*       INCLUDES       */
/************************/

	// shortcodes
	include TEMPLATE_DIRECTORY .'/inc/shortcodes/shortcodes.php';
	include TEMPLATE_DIRECTORY .'/inc/shortcodes/btc_arfolyamfigyelo.php';

/**/

/********************************/
/*         CSS & JQUERY         */
/********************************/

add_action('wp_enqueue_scripts', 'wiki_enqueue_scripts');
function wiki_enqueue_scripts() {

	/* CSS */
	//wp_enqueue_style('highlight_css', STYLESHEET_DIRECTORY .'/lib/highlight/styles/zenburn.css');
	wp_enqueue_style('icheck_css', STYLESHEET_DIRECTORY .'/lib/icheck/skins/minimal/aero.css');
	wp_enqueue_style('style_css', STYLESHEET_DIRECTORY .'/css/style.css');

	/* JS */
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap_js', TEMPLATE_DIRECTORY_URI. '/css/bootstrap/js/bootstrap.min.js', array('jquery') );
	//wp_enqueue_script( 'highlight_js', TEMPLATE_DIRECTORY_URI. '/lib/highlight/highlight.pack.js', array('jquery') );
	wp_enqueue_script( 'icheck_js', TEMPLATE_DIRECTORY_URI. '/lib/icheck/icheck.min.js', array('jquery') );
	wp_enqueue_script( 'site', TEMPLATE_DIRECTORY_URI. '/js/site.js', array('jquery') );
	wp_localize_script( 'site', 'wiki', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

/**/

/*************************/
/*    OTHER FUNCTIONS    */
/*************************/

function removal_accents($string) {
	$mit = array("á","é","í","ő","ő","ó","ü","ű","ú","Ö","Ü","Ó","Ő","Ú","É","Á","Ű");
	$mire = array("a","e","i","o","o","o","u","u","u","O","U","O","O","U","E","A","U");
	return str_replace($mit, $mire, $string);
}

function strtoupper_hu($text){
    $replace = array(
		'a' => 'A', 'á' => 'Á', 'b' => 'B', 'c' => 'C', 'cs' => 'Cs', 'd' => 'D', 'dz' => 'Dz', 'dzs' => 'Dzs', 'e' => 'E', 'é' => 'É', 'f' => 'F', 'g' => 'G', 'gy' => 'Gy', 'h' => 'H', 'i' => 'I', 'í' => 'Í', 'j' => 'J', 'k' => 'K', 'l' => 'L', 'ly' => 'Ly', 'm' => 'M', 'n' => 'N', 'ny' => 'Ny', 'o' => 'O', 'ó' => 'Ó', 'ö' => 'Ö', 'ő' => 'Ő', 'p' => 'P', 'q' => 'Q', 'r' => 'R', 's' => 'S', 'sz' => 'Sz', 't' => 'T', 'ty' => 'Ty', 'u' => 'U', 'ú' => 'Ú', 'ü' => 'Ü', 'ű' => 'Ű', 'v' => 'V', 'w' => 'W', 'x' => 'X', 'y' => 'Y', 'z' => 'Z', 'zs' => 'Zs');
    $text = str_replace(array_keys($replace), $replace, $text);
    return $text;
}

function string_convertTo_int($string) {
	if (!empty($string)) {
		preg_match_all('!\d+!', $string, $matches);
		return (int) join($matches[0]);
	}
}

// heti wp mentések linkjeinek összegyűjtése -> 1 gombra kattintva nyíljon meg minden admin oldal
function heti_wp_mentesek_admin_oldalak_megnyitasa() {
	$html="";
	$oldalak_array = array(
							"http://azendietam.hu/sajat-adatlap/",
							"http://mensmentisbabaprojekt.hu/wp-login.php",
							"http://www.biodental-horvath.hu/wp-login.php",
							"http://mensmentis.hu/wp-login.php",
							"http://pco-szindroma.hu/wp-login.php",
							"http://inzulinrezisztens.hu/wp-login.php",
							"http://mandulaliget.com/wp-login.php",
						);

	$html .= "<div id='heti_wp_mentesek_admin_oldalak_gomb'><button>Összes mentésre szánt oldal megnyitása</button><ul>";
				foreach ($oldalak_array as $key => $value) {
					$html .= "<li><a target='_blank' href='".$value."'>".$value."</a></li>";
				}
	return $html .= "</ul></div>";
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// csak az engedélyezett IP címekkel lehet az oldalt megnézni
function ip_ellenorzes_biztonsagi_function(){
    $get_client_ip = get_client_ip();

    $engedelyezett_ip_cimek_array = get_option( "engedelyezett_ip_cimek" );
    $talalat = false;
    $status = false;
    foreach ($engedelyezett_ip_cimek_array as $key => $value) {
        if ( ($value[0] ==  $get_client_ip) ) {
            $talalat = true;
            if ($value[1] == "true") { $status = true; }
        }
    }

    if ( $talalat === true ) {
        if ($status === true) {
            // teljesen engedélyezett IP

        } else {
            // bejegyzett, de még nem engedélyezett IP

            if ( isset($_GET['uj_ip_cim_hozzaadas']) && !empty($_GET['uj_ip_cim_hozzaadas']) ) {
                $uj_ip_cim = esc_attr($_GET['uj_ip_cim_hozzaadas']);
                foreach ($engedelyezett_ip_cimek_array as $key => $value) {
                    if ( $engedelyezett_ip_cimek_array[$key][0] == $uj_ip_cim ) {
                        $engedelyezett_ip_cimek_array[$key][1] = "true";
                    }
                }
                update_option( "engedelyezett_ip_cimek", $engedelyezett_ip_cimek_array );
                wp_redirect( wp_login_url() ); exit;
            } else {
                wp_logout();
                wp_redirect( "http://www.google.com" ); exit;
            }
        }
    } else {
        // új IP cím bejegyzése
        $engedelyezett_ip_cimek_array[] = array( $get_client_ip , "false");
        update_option( "engedelyezett_ip_cimek", $engedelyezett_ip_cimek_array );

        // üzenet küldése a WP adminnak
        $message = "Egy nem engedélyezett IP cím szeretett volna az oldalra belépni. \n\r".
            "Oldal URL: ". get_home_url() . "\n\r" .
            "IP cím: " . $get_client_ip . "\n\r \n\r" .
            "IP cím hozzáadása az engedélyezettek közé: " . wp_login_url()."/?uj_ip_cim_hozzaadas=".$get_client_ip;


			// csak akkor küldi el, ha magyarországról jön az IP
			$ipinfo = wp_remote_get('http://ipinfo.io/'.$get_client_ip.'/json');
			$ipinfo = wp_remote_retrieve_body( $ipinfo );
			$ipinfo = json_decode($ipinfo);

			if (!empty($ipinfo->country)) {
				if ( $ipinfo->country == 'HU' ) {
					wp_mail( get_option("admin_email"), "belépési kísérlet", $message );
				}
			}


        wp_logout();
        wp_redirect( "http://www.google.com" ); exit;
    }
}


function set_bejegyzes_nezettseg_frissites($postID = "") {
	if ( intval($postID) > 0 ) {  }
	else { return ""; }

	update_post_meta($postID, "utoljara_nezve", current_time( 'mysql' ));
	$count_key = 'megnezve';
	$count = get_post_meta($postID, $count_key, true);

	if ($count=='') {
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	} else {
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
	//echo "<input type='hidden' id='aktualis_oldal_nezetsege' value='".$count."' />";
}

function get_bejegyzes_nezettseg_frissites() {
	global $post;
	if ( !is_single($post->ID) ) { return ""; }

	echo "<input type='hidden' id='aktualis_oldal_nezetsege' value='". intval(get_post_meta($post->ID, 'megnezve', true)) ."' />";
}


// (szöveg, hossz)
function ShortTitle($text, $limit) {

	if (empty($limit)) { $chars_limit = 50; }
	else { $chars_limit = $limit; }

	$chars_text = strlen($text);
	$text = $text." ";
	$text = substr($text,0,$chars_limit);
	$text = substr($text,0,strrpos($text,' '));

	if ($chars_text > $chars_limit) {
		$text = $text."...";
	}
	return $text;
}

function bejegyzes_nezettseg_func() {
	get_bejegyzes_nezettseg_frissites();
}
//add_action( 'wp_footer', 'bejegyzes_nezettseg_func' );


// determine the topmost parent of a term
function get_term_top_most_parent($term_id, $taxonomy){
    // start from the current term
    $parent  = get_term_by( 'id', $term_id, $taxonomy);
    // climb up the hierarchy until we reach a term with parent = '0'
    while ($parent->parent != '0'){
        $term_id = $parent->parent;

        $parent  = get_term_by( 'id', $term_id, $taxonomy);
    }
    return $parent;
}

function get_the_content_by_id($post_id, $format = false) {
	$page_data = get_page($post_id);
	if ( $page_data ) {

	    if ( $format === true ) {
	      return apply_filters('the_content',$page_data->post_content);
	    } else {
	        return $page_data->post_content;
	    }
	}
	return false;
}

function get_the_excerpt_by_id($post_id, $text_length) {
	global $post;
	$save_post = $post;
	$post = get_post($post_id);
	$output = $post->post_excerpt;
	$post = $save_post;

	if ( empty($text_length) ) { $text_length = 55; }

	if($output == ''){
	    $output = get_the_content_by_id($post_id);
	    $output = strip_shortcodes( $output );
	    $output = apply_filters('the_content', $output);
	    $output = str_replace(']]>', ']]>', $output);
	    $output = strip_tags($output);
	    $output = nl2br($output);
	    $excerpt_length = apply_filters('excerpt_length', $text_length);
	    $words = explode(' ', $output, $excerpt_length + 1);
	    if (count($words) > $excerpt_length) {
	        array_pop($words);
	        array_push($words, '...');
	        $output = implode(' ', $words);
	    }
	}
	return $output;
}

function get_most_viewed_posts($return_num) {
	if ( empty($return_num) ) { $return_num = 10; }

	$args = new WP_Query(array( 'post_type' => 'post',
								'fields' => 'ids',
								'posts_per_page' => $return_num,
								'meta_key' => 'megnezve',
								'orderby' => 'meta_value_num',
								'order' => 'DESC'
							));
	return $args->posts; // array
}


function tombrendezo($kapott_array, $index_ertek, $osszevonando_ertek, $kimenetel, $mellekes_adatok_array) {

	//--------------- index értékek előállítása ------------------
		if (empty($kimenetel)) { return ""; }
		if (empty($index_ertek) || empty($osszevonando_ertek)) { return ""; }
	//-----------------------------------------
		/*// egyedi érték ami alapján keressen
		$index_ertek = 'datum';

		// amiket össze kell szedni és az indexekhez adni
		$osszevonando_ertek = 'osszeg';*/
	//-----------------------------------------

	// index(egyedi) értékek kigyűjtése
	$index_array = array();
	foreach ($kapott_array as $key => $value) {
		array_push($index_array, $value[$index_ertek]);
	}

	// egyedi index értékek (duplikátumok megszüntetése)
	$index_array = array_unique($index_array);

	// tömb sorbarendezése
	sort($index_array);

	//echo "<pre>";var_dump($index_array);echo "</pre></hr>";

	//-------------------------------------------------------------

	// minden egyedi dátumhoz hozzápárosítja az értékeit külön, külön
	$hozzaadott_ertekek_array = array();
	foreach ($index_array as $key => $value) {
	    for ($i=0; $i < count($kapott_array); $i++) {
	        if ($kapott_array[$i][$index_ertek] == $value) {
	            $hozzaadott_ertekek_array[$value][] = $kapott_array[$i][$osszevonando_ertek];

		            // további tömb értékek hozzáadása (ha van megadva)
		            if (isset($mellekes_adatok_array) && !empty($mellekes_adatok_array)) {
		            	for ($j=0; $j < count($mellekes_adatok_array); $j++) {
		            		$hozzaadott_ertekek_array[$value][][$mellekes_adatok_array[$j]] = $kapott_array[$i][$mellekes_adatok_array[$j]];

		            	}
		            }
	        }
	    }
	}

	//echo "<pre>";var_dump($hozzaadott_ertekek_array);echo "</pre></hr>";

	if ($kimenetel == 'gyujtes') {

		return $hozzaadott_ertekek_array;
	}
	//--------------------------------------------------------------
	else if ($kimenetel == 'osszevonas') {

		// összegek összeszámolása az egyedi dátumok(kulcs) alapján
		$osszevont_ertekek_array = array();
		foreach ($hozzaadott_ertekek_array as $key => $value) {
		    array_push($osszevont_ertekek_array, array($index_ertek => $key, $osszevonando_ertek => array_sum($value)));
		}

		//echo "<pre>";var_dump($osszevont_ertekek_array);echo "</pre></hr>";

		return $osszevont_ertekek_array;
	}

	//--------------------------------------------------------------

}


/**/


/**************************/
/*		   Sidebars	      */
/**************************/

add_action( 'widgets_init', 'wiki_widgets_init' );
function wiki_widgets_init() {

	register_sidebar(array(
		'name' => 'Default Sidebar',
		'id' => 'default_sidebar',
		'class' => '',
		'description' => '',
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="wg-title"><span>',
		'after_title' => '</span></h2>'
	));

}

/**/


/************************/
/*       METABOX        */
/************************/

// blogbejegyzésekhez hozzárendelhető galéria id-k
add_action( 'add_meta_boxes', 'wiki_custom_meta_box' );
function wiki_custom_meta_box($post) {
	add_meta_box('useful_plugin_link', 'Useful Plugin link', 'useful_plugin_link_metabox_function', 'useful_plugins_cpt', 'normal' , 'default');
}

function useful_plugin_link_metabox_function($post) {
	$useful_plugin_url = get_post_meta($post->ID, 'useful_plugin_url', true);

	echo
		"<table style='width: 100%;'>
		    <tr style='width: 100%;'>
		        <th style='width: 8%;'>URL: </th>
		        <td style='width: 92%;'><input style='width: 100%;' type='text' name='useful_plugin_url' value='".$useful_plugin_url."' /></td>
		    </tr>
		</table>";
}

/**/


/*************************************/
/*    SAVE POST / METABOX / etc...   */
/*************************************/

add_action('save_post', 'wiki_save_custom_postdata');
function wiki_save_custom_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
	    return;
	global $post;

	if ($_POST['post_type'] == "useful_plugins_cpt") {
		update_post_meta($post->ID, "useful_plugin_url", $_POST['useful_plugin_url']);
	}
}

/**/


/*******************/
/*      CPT        */
/*******************/

add_action( 'init', 'wiki_post_product' );
function wiki_post_product() {

	// Képméretező CPT
	$labels = array(
		'name' 					=> 'Képméretező',
		'singular_name' 		=> 'Képméretező',
		'add_new' 				=> 'Új kép hozzáadása',
		'add_new_item' 			=> 'Új kép hozzáadása',
		'edit_item' 			=> 'kép szerkesztése',
		'new_item' 				=> 'Új kép',
		'view_item' 			=> 'Kép megtekintése',
		'search_items' 			=> 'Kép keresése',
		'not_found' 			=> 'Nincs ilyen kép',
		'not_found_in_trash' 	=> 'Nincs kép a lomtárban',
		'parent_item_colon' 	=> ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'menu_icon' => 'dashicons-format-gallery',
		'supports' => array( 'title', 'custom-fields' )
	);
	register_post_type('kepmeretezo_cpt', $args);

	// FTP kapcsolatok CPT
	$labels = array(
		'name' 					=> 'FTP Kapcsolatok',
		'singular_name' 		=> 'FTP Kapcsolat',
		'add_new' 				=> 'Új FTP hozzáadása',
		'add_new_item' 			=> 'Új FTP hozzáadása',
		'edit_item' 			=> 'FTP szerkesztése',
		'new_item' 				=> 'Új FTP',
		'view_item' 			=> 'FTP megtekintése',
		'search_items' 			=> 'FTP keresése',
		'not_found' 			=> 'Nincs ilyen FTP',
		'not_found_in_trash' 	=> 'Nincs FTP a lomtárban',
		'parent_item_colon' 	=> ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'menu_icon' => 'dashicons-lock',
		'supports' => array( 'title', 'editor' )
	);
	register_post_type('ftp_kapcsolatok_cpt', $args);

    // WP adminok CPT
    $labels = array(
        'name'                  => 'WP Admin loginok',
        'singular_name'         => 'WP Admin login',
        'add_new'               => 'Új Login hozzáadása',
        'add_new_item'          => 'Új Login hozzáadása',
        'edit_item'             => 'Login szerkesztése',
        'new_item'              => 'Új Login',
        'view_item'             => 'Login megtekintése',
        'search_items'          => 'Login keresése',
        'not_found'             => 'Nincs ilyen Login',
        'not_found_in_trash'    => 'Nincs Login a lomtárban',
        'parent_item_colon'     => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'menu_icon' => 'dashicons-lock',
        'supports' => array( 'title', 'editor' )
    );
    register_post_type('wp_adminok_cpt', $args);

		// useful plugins
    $labels = array(
        'name'                  => 'Useful Plugins',
        'singular_name'         => 'Useful Plugins',
        'add_new'               => 'Új Plugin hozzáadása',
        'add_new_item'          => 'Új Plugin hozzáadása',
        'edit_item'             => 'Plugin szerkesztése',
        'new_item'              => 'Új Plugin',
        'view_item'             => 'Plugin megtekintése',
        'search_items'          => 'Plugin keresése',
        'not_found'             => 'Nincs ilyen Plugin',
        'not_found_in_trash'    => 'Nincs Plugin a lomtárban',
        'parent_item_colon'     => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );
    register_post_type('useful_plugins_cpt', $args);

}

/**/


/**************************/
/*      Taxonomies        */
/**************************/

add_action( 'init', 'wiki_custom_taxonomies', 0 );
function wiki_custom_taxonomies(){

	register_taxonomy(
		'useful_plugins_category',
		'useful_plugins_cpt',
		array(
			'hierarchical' => true,
			'label' => 'Categories',
			'show_admin_column'  => true
		)
	);

}

/**/



/*********************/
/*      Roles        */
/*********************/



/**/


/*******************************/
/*          ADD ACTION         */
/*******************************/

//add_action('init', 'wiki_restricted_site_access');
function wiki_restricted_site_access() {
	if ( !is_user_logged_in() && !is_admin() ) {
		die;
	}
}


// Regenerate all posts featured images
add_action('init', 'bejegyzes_thumbnaik_ujrageneralas_function');
function bejegyzes_thumbnaik_ujrageneralas_function(){
	global $post;
	if(isset($_POST["bejegyzes_thumbnaik_ujrageneralas"])) {
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'post',
		);
		$posts_array = get_posts( $args );

		foreach ($posts_array as $post) {

			if (strpos($post->post_content, "wp-image-") !== false) {

				// első beszúrt kép ID kinyerése a post-ból
				$image_id = substr($post->post_content, strpos($post->post_content, "wp-image-"), 25);
				$image_id = string_convertTo_int($image_id);

					// létezik -e még a kép a médiatárban
					$array = wp_get_attachment_image_src( $image_id );
					if (!empty($array[0])) {

						// beállítás thumbnail képnek
						if ( !has_post_thumbnail( $post->ID ) ) {
							set_post_thumbnail( $post->ID, $image_id );
						}
					}
			}
		}
	}
}

add_action('admin_menu', 'theme_admin_pages');
function theme_admin_pages() {
	add_menu_page( 'Egyedi beállítások', 'Egyedi beállítások', 'manage_options', 'egyedi_beallitasok', 'egyedi_beallitasok_settings_page', 'dashicons-admin-generic');
}

/**/


/********************************/
/*           FILTERS            */
/********************************/


/* PageNavi to Bootstrap nav */
add_filter( 'wp_pagenavi', 'ec_pagination', 10, 2 );
function ec_pagination($html) {
    $out = '';

    //wrap a's and span's in li's
    $out = str_replace("<div","",$html);
    $out = str_replace("class='wp-pagenavi'>","",$out);
    $out = str_replace("<a","<li><a",$out);
    $out = str_replace("</a>","</a></li>",$out);
    $out = str_replace("<span","<li><span",$out);
    $out = str_replace("</span>","</span></li>",$out);
    $out = str_replace("</div>","",$out);

    return '<nav class="pagination">
                <ul class="pagination">'.$out.'</ul>
            </nav>';
}


// Enable Shortcode In Widget
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode', 11);


add_action( 'init', 'update_frontpage_post_list_order' );
function update_frontpage_post_list_order() {
	if ( !is_admin() ) {
		$frontpage_post_list_order = get_option( 'frontpage_post_list_order' );

		// update every 3 days
		if( $frontpage_post_list_order['timestamp'] <= time() - (3 * 86400) ) {


			// legnézetteb bejegyzések lekérése
			$most_viewed_posts_array = get_most_viewed_posts(100);

			// kategóriák kigyűjtése
			$terms_array = array();
			foreach ($most_viewed_posts_array as $key => $post_id) {
				$terms = wp_get_post_terms( $post_id, 'category' );

				foreach ($terms as $key => $term_single) {
					if ( $term_single->parent == 0 ) {
						$terms_array []= (int) $term_single->term_id;
					}
				}
			}
			$terms_array = array_values(array_filter(array_unique($terms_array)));


			// listázás összeállítás

			/* eredeti lekérés
			$terms_array = get_terms( 'category', array(
									    'orderby' => 'count',
									    'order'	=> 'DESC',
									    'hide_empty' => TRUE,
									    'parent' => 0
									) );
			*/
			$terms = array_slice($terms_array, 0, 12);

			$main_terms_array = array();
			foreach ( $terms as $term_id ) {
				$category = get_term_by('id', $term_id, 'category');

				$main_terms_array []= array(
											'term_id' => $category->term_id,
											'term_url' => get_term_link( $category ),
											'term_name' => $category->name,
											);
			}
			$main_terms_array = array_chunk($main_terms_array, 3);

			// eredmény mentése

			update_option( 'frontpage_post_list_order', array( 'timestamp' => time(), 'result' => $main_terms_array ) );


			// teszt email
			//mail('paulovicstibor@gmail.com', 'wiki oldal', 'update_frontpage_post_list_order');
		}
	}
}

/**/


/*******************************/
/*          AJAX 	           */
/*******************************/


add_action( 'wp_ajax_bejegyzes_nezettseg_frissites', 'wiki_bejegyzes_nezettseg_frissites' );
add_action( 'wp_ajax_nopriv_bejegyzes_nezettseg_frissites', 'wiki_bejegyzes_nezettseg_frissites' );
function wiki_bejegyzes_nezettseg_frissites() {
	$post_id = (int) $_POST['post_id'];
	$post_type = get_post_type($post_id);

	if ( 	($post_type == 'useful_plugins_cpt') ||
				($post_type == 'post') ||
				($post_type == 'page') ||
				($post_type == 'kepmeretezo_cpt') ||
				($post_type == 'ftp_kapcsolatok_cpt') ||
				($post_type == 'wp_adminok_cpt')
			) {

		set_bejegyzes_nezettseg_frissites($post_id);
		echo json_encode(array('ready' => TRUE, 'hash' => $_POST['hash']));
	} else {
		echo json_encode(array('ready' => FALSE, 'hash' => $_POST['hash']));
	}
	exit;
}


add_action( 'wp_ajax_search_results', 'wiki_search_results' );
add_action( 'wp_ajax_nopriv_search_results', 'wiki_search_results' );
function wiki_search_results() {
	global $wpdb;

	$colors_array = array( 	'ffe100', 'efff00', 'ffbf00', 'f7ff7a', 'ffde7a', 'fdffdc',
													'ffe100', 'efff00', 'ffbf00', 'f7ff7a', 'ffde7a', 'fdffdc',
													'ffe100', 'efff00', 'ffbf00', 'f7ff7a', 'ffde7a', 'fdffdc' );


	$search_value = $_POST['search_value'];
	if ( empty($search_value) ) { return ""; }


	/* beírt keresési szavak kiegészítése - ékezet nélküli és kisbetűs vált., ... */

	$gyorsker_array_temp = array();
	$gyorsker_array = explode(" ", $search_value);

	foreach ($gyorsker_array as $key => $val) {
		if ( !empty($val) ) {
			$gyorsker_array_temp []= removal_accents($val);
			$gyorsker_array_temp []= strtolower($val);
			$gyorsker_array_temp []= strtoupper_hu($val);
			$gyorsker_array_temp []= strtoupper_hu($val[0]) . strtolower(substr($val, 1));
		}
	}
	$gyorsker_array = array_merge($gyorsker_array,$gyorsker_array_temp);
	$gyorsker_array = array_values(array_filter(array_unique($gyorsker_array)));
	unset($gyorsker_array_temp);
	/* --- */


	/* KERESÉSI FELTÉTELEK ALAPJÁN ÖSSZEÁLLÍTJA A LEKÉRDEZÉST */
	$search_terms = $_POST['search_terms'];
	$results_post_array = array();


		/* Cím alapján keresés */
		if ( $search_terms['post_cim'] == 'true' ) {

			$gyorsker_string = "";
			if ( count($gyorsker_array) > 1 ) {
				foreach($gyorsker_array as $key => $val) {
					if ($key == 0) { $and = ""; }
					else { $and = "AND"; }

					$gyorsker_string .= " ".$and." post_title LIKE '%".$val."%'";
				}
				$gyorsker_string = "OR {$gyorsker_string}";
			}

			$posts = $wpdb->get_results( "SELECT ID
											FROM $wpdb->posts
											WHERE post_title LIKE '%{$search_value}%' {$gyorsker_string} " );

			foreach ($posts as $key => $post) {
				$results_post_array []= (int) $post->ID;
			}
			$results_post_array = array_values(array_filter(array_unique($results_post_array)));
		}

		/* Kulcsszavak alapján keresés */
		if ( $search_terms['keresoszavak'] == 'true' ) {

			$gyorsker_string = "";
			if ( count($gyorsker_array) > 1 ) {
				foreach($gyorsker_array as $key => $val) {
					if ($key == 0) { $and = ""; }
					else { $and = "AND"; }

					$gyorsker_string .= " ".$and." wiki375terms.name LIKE '%".$val."%'";
				}
				$gyorsker_string = "OR {$gyorsker_string}";
			}

			$terms = $wpdb->get_results( "SELECT wiki375terms.term_id FROM wiki375terms
											LEFT JOIN wiki375term_taxonomy
											ON wiki375terms.term_id = wiki375term_taxonomy.term_id
												WHERE wiki375term_taxonomy.taxonomy = 'post_tag'
														AND wiki375terms.name LIKE '%{$search_value}%' {$gyorsker_string} " );
			foreach ($terms as $key => $term) {
				$tag_id = (int) $term->term_id;

				$posts_cpt = new WP_Query(array( 'post_type' => 'post',
													'fields' => 'ids',
													'posts_per_page' => 1,
													'tag__in' => array( $tag_id )
											));
				$posts_array = $posts_cpt->posts;
				$results_post_array []= (int) $posts_array[0];
			}

			$results_post_array = array_values(array_filter(array_unique($results_post_array)));
		}


		/* végső filter */
		/* Kulcsszavak alapján keresés */
		if ( $search_terms['prog_kapcs'] == 'true' || $search_terms['cgi_content'] == 'true' ) {

			$blender_posts_cpt = new WP_Query(array( 'post_type' => 'post',
			                                    'fields' => 'ids',
			                                    'posts_per_page' => -1,
			                                    'tax_query' => array(
			                                            array(
			                                                'taxonomy' => 'category',
			                                                'field'    => 'term_id',
			                                                'terms'    => array( BLENDER_TERM_ID ),
			                                            ),
			                                        ),
			));
			$blender_posts_array = $blender_posts_cpt->posts;

			$results_post_array_temp = array();

			foreach ($results_post_array as $key => $post_id) {

				if ( $search_terms['prog_kapcs'] == 'true' ) {
					if ( !in_array($post_id, $blender_posts_array) ) {
						$results_post_array_temp []= (int) $post_id;
					}
				}
				else if ( $search_terms['cgi_content'] == 'true' ) {
					if ( in_array($post_id, $blender_posts_array) )	{
						$results_post_array_temp []= (int) $post_id;
					}
				} else {
					$results_post_array_temp []= (int) $post_id;
				}
			}
			$results_post_array = $results_post_array_temp;
			unset($results_post_array_temp);
		}


		/* Tartalom alapján keresés */
		if ( $search_terms['tartalom'] == 'true' ) {

			$gyorsker_string = "";
			if ( count($gyorsker_array) > 1 ) {
				foreach($gyorsker_array as $key => $val) {
					if ($key == 0) { $and = ""; }
					else { $and = "AND"; }

					$gyorsker_string .= " ".$and." post_content LIKE '%".$val."%'";
				}
				$gyorsker_string = "OR {$gyorsker_string}";
			}

			$posts = $wpdb->get_results( "SELECT ID
											FROM $wpdb->posts
											WHERE post_content LIKE '%{$search_value}%' {$gyorsker_string} " );

			foreach ($posts as $key => $post) {
				$results_post_array []= (int) $post->ID;
			}
			$results_post_array = array_values(array_filter(array_unique($results_post_array)));
		}

	/* /KERESÉSI FELTÉTELEK ALAPJÁN ÖSSZEÁLLÍTJA A LEKÉRDEZÉST */



	/* highlight colors */
	$search_value_array = $gyorsker_array;
	$li = "";
	foreach ($search_value_array as $key => $value) {
		$li .= '<li data-search-term="'. strtolower($value) .'" data-color="'. $colors_array[$key] .'"></li>';
	}
	$colored_search_terms = '<ul id="colored_search_terms" class="">'. $li .'</ul>';

	// ---

	$result = "";
	/*
	$results_post = new WP_Query(array( 'post_type' 	 => 'post',
										'fields' 		 => 'ids',
										'posts_per_page' => 50,
										'meta_key'   	 => 'megnezve',
										'orderby'    	 => 'meta_value_num',
										'order'      	 => 'DESC',

										$filters
									));
	$results_post_array = $results_post->posts;*/

	$results_post_array_temp = array();
	foreach ($results_post_array as $key => $post_id) {
		$terms = wp_get_post_terms( $post_id, 'category' );

		foreach ($terms as $key => $term) {
			$results_post_array_temp []= array( 'post_id' => $post_id, 'term_id' => $term->term_id );
		}
	}
	$results_post_array = tombrendezo( $results_post_array_temp, 'term_id', 'post_id', 'gyujtes', array() );
	unset( $results_post_array_temp );


	foreach ($results_post_array as $term_id => $posts_array) {
		$term = get_term_by('id', $term_id, 'category');

		$result .= '
				<div class="panel panel-default" data-term-id="'. $term->term_id .'">
				  <div class="panel-heading">
				    <h3 class="panel-title">'. $term->name .'</h3>
				  </div>
				  <div class="panel-body">
					<ul class="list-group">';

						$li = "";
						foreach ($posts_array as $key => $post_id) {

							$post_title = get_the_title( $post_id );

							// ---  Highlighting the headings ---

							$str = $post_title;
							$keyword = $search_value;
							$keyword = implode('|',explode(' ',preg_quote($keyword)));
							$str = preg_replace("/($keyword)/i",'<b class="highlight">$0</b>',$str);
							$post_title = $str;

							// ---


							$li .= '<li class="list-group-item">
										<span class="badge views">'. get_post_meta( $post_id, 'megnezve', true ) .'</span>'.
										'<a href="'.get_permalink($post_id).'">'. $post_title .'</a>
									</li>';
						}
		$result .= 		$li;
		$result .= '
					</ul>
				  </div>
				</div>';
	}

	echo json_encode(array('html' => $result, 'hash' => $_POST['hash'], 'colored_search_terms' => $colored_search_terms));
    exit;
}

/**/
