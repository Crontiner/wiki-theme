<?php

function btc_percent($num, $amount){
  $num = (int) str_replace('.', '', $num);
  $amount = (int) str_replace('.', '', $amount);
  //return (int) ($num * $amount) / 100;
  return (int) round( ($num * $amount) / 100 ,0,PHP_ROUND_HALF_UP);
}
