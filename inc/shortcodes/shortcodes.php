<?php

add_shortcode('useful_plugins_list', 'useful_plugins_list_sc_function');
function useful_plugins_list_sc_function($atts, $content = null) {

	$useful_plugins_cpt = new WP_Query(array( 'post_type' => 'useful_plugins_cpt',
				                                    'fields' => 'ids',
				                                    'posts_per_page' => -1,
	));
	$useful_plugins_posts = $useful_plugins_cpt->posts;

	$li = "";
	foreach ($useful_plugins_posts as $key => $post_id) {
		$useful_plugin_url = get_post_meta($post_id, 'useful_plugin_url', true);
		$found_img_url = get_post_meta($post_id, 'found_img_url', true);
		$expired_plugin = (bool) get_post_meta($post_id, 'expired_plugin', true);

		$bg_image = "";
		$bg_image_class = "";
		if ( has_post_thumbnail($post_id) ) {

			$array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
			if ( !empty($array[0]) ) { $bg_image = $array[0]; }

		} else if ( !empty($found_img_url) ) {
			$bg_image = $found_img_url;
		}


		// Search in web
		if ( empty($bg_image) && !empty($useful_plugin_url) && empty($found_img_url) ) {
			$pattern_start 	= '<meta property="og:image" content="';
			$pattern_end 		= '" />';
			$link_pattern = 'wordpress.org/plugins/';

			if (strpos($useful_plugin_url, $link_pattern) !== false) {
				$html = file_get_contents( $useful_plugin_url );

				// IF: This plugin has been closed and is no longer available for download.
				if (strpos($html, 'notice-error') !== false) {
					update_post_meta($post_id, 'expired_plugin', TRUE);
				}

				if (strpos($html, $pattern_start) !== false) {
					$html = substr($html, strpos($html, $pattern_start) + strlen($pattern_start) );
					$html = substr($html, 0, strpos($html, $pattern_end));
					$html = substr($html, 0, strpos($html, '?'));

					if ( !empty($html) ) {
						$bg_image = $html;
						update_post_meta($post_id, 'found_img_url', $bg_image);
					} else {
						update_post_meta($post_id, 'found_img_url', "#");
					}
				} else {
					update_post_meta($post_id, 'found_img_url', "#");
				}
			} else {
				update_post_meta($post_id, 'found_img_url', "#");
			}
		}

		if ( !empty($bg_image) ) {
			$bg_image = 'background-image: url('. $bg_image .');';
			$bg_image_class = "has_bg_image";
		}

		$expired_plugin_class = "";
		if ( $expired_plugin === TRUE ) {
			$expired_plugin_class = "expired_plugin";
		}

		$excerpt = ShortTitle(get_the_content_by_id($post_id),500);


		$li .= 	'<li style="'. $bg_image .'" class="'. $bg_image_class .' '. $expired_plugin_class .' ">'.
							'<a class="post-title" target="_blank" href="'. $useful_plugin_url .'">'.
								'<h3>'. get_the_title($post_id) .'</h3>'.
							'</a>'.
							'<div class="excerpt">'. $excerpt .'</div>'.
							'<a class="read_more" target="_blank" href="'. get_permalink($post_id) .'">Bővebben</a>'.
						'</li>';
	}

	return '<ul class="useful_plugins_list_sc">'. $li .'</ul>';
}


/*add_shortcode('php', 'php_sc_function');
function php_sc_function($atts, $content = null) {
	return '<pre>'. do_shortcode($content) .'</pre>';
}*/

function legutobb_nezett_bejegyzesek_sc_function() {
	global $post, $wpdb;
	$legutobb_nezett_bejegyzesek = $wpdb->get_results( "SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = 'utoljara_nezve' ORDER BY meta_value DESC LIMIT 40", ARRAY_A );

	$legutobb_nezett_bejegyzesek_id = array();
	foreach ($legutobb_nezett_bejegyzesek as $key => $value) {
		$post_id = $value['post_id'];
		if (!empty($post_id) && (get_post_status( $post_id ) == 'publish' ) && ($post_id != FRONT_PAGE_ID) ) {
			//if ( get_post_type( $post_id ) == 'post' ) {
				$legutobb_nezett_bejegyzesek_id[] = $post_id;
			//}
		}
	}

	array_splice($legutobb_nezett_bejegyzesek_id, 10);

	$li="";
	$post_id="";
	foreach ($legutobb_nezett_bejegyzesek_id as $key => $value) {
		$post_id = $value;
		if (!empty($post_id) && (get_post_status( $post_id ) == 'publish' ) ) {
			$li .=  "<li><a href='".get_permalink($post_id)."'>".ShortTitle( get_the_title($post_id) , 50)."</a></li>";
		}
	}
	return "<ul id='legutobb_nezett_bejegyzesek_widget'>".$li."</ul>";
}
add_shortcode('legutobb_nezett_bejegyzesek', 'legutobb_nezett_bejegyzesek_sc_function');

function heti_wp_mentesek_nevei_sc_function() {
	$mainap = date("Y.m.d");
	$oldalak = array(
					"azendietam_complete_backup_".$mainap.".zip",
					"mensmentisbabaproject_complete_backup_".$mainap.".zip",
					"biodental_complete_backup_".$mainap.".zip",
					"inzulinrezisztens_complete_backup_".$mainap.".zip",
					"mandulaliget_complete_backup_".$mainap.".zip",
					"mensmentis_complete_backup_".$mainap.".zip",
					"pco-szindroma.hu_complete_backup_".$mainap.".zip",
					"pivot270_complete_backup_".$mainap.".zip"
				);
	$eredm = "";
	foreach ($oldalak as $key => $oldal) {
		$eredm .= "zip -r ".$oldal." ./*" . "<br />";
	}
	return $eredm . heti_wp_mentesek_admin_oldalak_megnyitasa();
}
add_shortcode('heti_wp_mentesek_nevei', 'heti_wp_mentesek_nevei_sc_function');

function legt_nezett_bejegyz_sc_function() {
	global $post, $wpdb;

	$args = new WP_Query(array( 'post_type' => 'post',
								'fields' => 'ids',
								'posts_per_page' => 10,
								'meta_key' => 'megnezve',
								'orderby' => 'meta_value_num',
								'order' => 'DESC'
							));
	$nezetseg_adatok = $args->posts;

	// összes bejegyzés megnézéseinek összeszámolása
	$bejegyzes_nezettsegek_osszege = $wpdb->get_results( "SELECT SUM(meta_value) AS osszeg FROM ".$wpdb->prefix."postmeta WHERE meta_key = 'megnezve' ", ARRAY_A );
	$osszes_megtekintes = "<div class='osszes_megtekintes'>Összes bejegyzés megtekintés: ". $bejegyzes_nezettsegek_osszege[0]['osszeg'] . "</div>";
	//----------------------------------------------

	$eredmeny="";
	foreach ($nezetseg_adatok as $key => $post_id) {
		if (!empty($post_id)) {
			$megnezve = get_post_meta( $post_id, "megnezve", true);
			$eredmeny .= "<li><a href='".get_permalink( $post_id )."'>".ShortTitle( get_the_title($post_id) , 35)." <span>".$megnezve."</span></a></li>";
		}
	}
	return "<ul class='legtobbet_nezett_postok_widget'>". $eredmeny ."</ul>" . $osszes_megtekintes;
}
add_shortcode('legtobbet_nezet_bejegyzesek_lista_widget', 'legt_nezett_bejegyz_sc_function');


function legtobbet_nezet_bejegyzesek_lista_sc_function() {
	global $post, $wpdb;

	$args = array( 'posts_per_page' => -1, 'meta_key' => 'megnezve', 'orderby' => 'meta_value_num', 'order' => 'DESC'  );
	$nezetseg_adatok = get_posts( $args );

	$eredmeny="";
	foreach ($nezetseg_adatok as $key => $value) {
		if (!empty($value->ID)) {
			$megnezve = get_post_meta( $value->ID, "megnezve", true);
			$eredmeny .= "<li><a href='".get_permalink( $value->ID )."'>".$value->post_title." <span>".$megnezve."</span></a></li>";
		}
	}
	return "<ol class='legtobbet_nezett_postok_lista'>". $eredmeny ."</ol>";
}
add_shortcode('legtobbet_nezet_bejegyzesek_lista', 'legtobbet_nezet_bejegyzesek_lista_sc_function');


function slug_generator_sc_function() {
	$result = "";

	if ( isset($_POST['slug_generator_textarea']) ) {

		$slug_generator_textarea = explode( PHP_EOL ,$_POST['slug_generator_textarea'] );
		$slug_generator_textarea_temp = array();
		foreach ($slug_generator_textarea as $key => $value) {
			$slug_generator_textarea_temp[] = sanitize_title( $value );
			$slug_generator_textarea_temp[] = str_replace('-', '_', sanitize_title( $value ));
			$slug_generator_textarea_temp[] = '&#13;';
		}

		$result .= '<textarea class="eredmeny">'.implode( '&#13;&#10;' , $slug_generator_textarea_temp).'</textarea>';
	}

	$result .= '
	<form action="" method="post">
		<textarea class="feltetel" name="slug_generator_textarea">'.$_POST['slug_generator_textarea'].'</textarea>
		<div class="clear"></div>
		<input type="submit" value="Generálás" name="slug_generator_submit" />
	</form>';

	return '<div id="slug_generator_sc">'.$result.'</div>';
}
add_shortcode('slug_generator', 'slug_generator_sc_function');


function kepmeretezo_sc_function() {
	global $gallery_plugin_timthumb_url;
	$result = "";

	$img_width = get_post_meta( KEPMERETEZO_POST_ID, 'img-width', true );
	$img_height = get_post_meta( KEPMERETEZO_POST_ID, 'img-height', true );

	if ( !empty($img_width) ) { $img_width = '&w='.$img_width; } else { $img_width = ""; }
	if ( !empty($img_height) ) { $img_height = '&h='.$img_height; } else { $img_height = ""; }

	$gallery_items = get_gallery_plugin_post_attachments( KEPMERETEZO_POST_ID, 'img' );
	foreach ($gallery_items as $key => $img_id) {
		$array = wp_get_attachment_image_src( $img_id, 'full' );
		$img_url = $array[0];

		$result .= '<img src="'.$gallery_plugin_timthumb_url . $img_url . $img_width.$img_height . get_timthumb_metabox_settings( KEPMERETEZO_POST_ID ) .'" />';
	}

	return $result;
}
add_shortcode('kepmeretezo_sc', 'kepmeretezo_sc_function');



add_shortcode('ftp_kapcsolatok_lista', 'ftp_kapcsolatok_lista_sc_function');
function ftp_kapcsolatok_lista_sc_function() {
	$ftp_kapcsolatok_cpt = new WP_Query(array( 'post_type' => 'ftp_kapcsolatok_cpt',
	                                    'fields' => 'ids',
	                                    'posts_per_page' => -1,
	));
	$ftp_kapcsolatok_array = $ftp_kapcsolatok_cpt->posts;

	$li = '';

	foreach ($ftp_kapcsolatok_array as $key => $post_id) {
		$li .= '<li class="list-group-item"><a href="'. get_permalink( $post_id ) .'" target="_blank">'. get_the_title( $post_id ) .'</a></li>';
	}

	return '<ul class="list-group">'. $li .'</ul>';
}

add_shortcode('wp_adminok_lista', 'wp_adminok_lista_sc_function');
function wp_adminok_lista_sc_function() {
    $ftp_kapcsolatok_cpt = new WP_Query(array( 'post_type' => 'wp_adminok_cpt',
                                        'fields' => 'ids',
                                        'posts_per_page' => -1,
    ));
    $ftp_kapcsolatok_array = $ftp_kapcsolatok_cpt->posts;

    $li = '';

    foreach ($ftp_kapcsolatok_array as $key => $post_id) {
        $li .= '<li class="list-group-item"><a href="'. get_permalink( $post_id ) .'" target="_blank">'. get_the_title( $post_id ) .'</a></li>';
    }

    return '<ul class="list-group">'. $li .'</ul>';
}
