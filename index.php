<?php get_header(); ?>

	<div id="page_content" class="col-sm-9">

		<section id="search_results">
			<!-- Search results here... -->
		</section>

		<div class="page-content-section">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					$postID = get_the_ID();
					$megnezve = get_post_meta($postID, 'megnezve', true);
					if (empty($megnezve)) { $megnezve = 0; }
					?>
					<article>
						<div class="header">
							<h2 class="title"><?php echo get_the_title(); ?></h2>
							<div class="post_info">
								<span class="post-date"><?php echo get_the_date(); ?></span>
								<span class="views"><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> <?php echo $megnezve; ?></span>
							</div>
						</div>
						<div class="desc">
							<?php the_content(); ?>
							<div class="clearfix"></div>
						</div>
					</article>
					<?php
				} // end while
			} // end if
			?>
		</div>
	</div>

	<?php get_template_part('template-parts/sidebar'); ?>

<?php get_footer(); ?>
